
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

function poisciDrugje() {
	document.getElementById("poisciKrvodajalca").style.display = "block";
}

function hidePoisciKrvodajalca() {
	document.getElementById("poisciKrvodajalca").style.display = "none";
} 

function prikaziMoznostVnosa() {
	document.getElementById("novVnos").style.visibility = "visible";
}

function hideStatistika() {
	document.getElementById("statistika").style.display = "none";
} 

function hideOsnovno() {
	document.getElementById("osnovno").style.display = "none";
} 

function prikaziStatistiko() {
	document.getElementById("statistika").style.display = "block";
	izris();
}

function izris() {
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(drawChart1);
	google.charts.setOnLoadCallback(drawChart2);
	
	function drawChart1() {
	  var data = google.visualization.arrayToDataTable([
	  ['Krvna skupina', 'Pogostost'],
	  ['A', 40],
	  ['B', 15],
	  ['AB', 7],
	  ['0', 38],
		]);
	  var options = {'title':'Zastopanost krvnih skupin v Sloveniji', 
					'width':500, 'height':280,
	  				chartArea: {
				        width: '80%',
				        height: '80%',
				      },
					};
	
	  var chart = new google.visualization.PieChart(document.getElementById('piechart1'));
	  chart.draw(data, options);
	}	
	
	function drawChart2() {
	  var data = google.visualization.arrayToDataTable([
	  ['Krvna skupina', 'Pogostost'],
	  ['A', typeA],
	  ['B', typeB],
	  ['AB', typeAB],
	  ['0', type0],
		]);
	  var options = {'title':'Zastopanost krvnih skupin v naši bazi', 
					'width':500, 'height':280,
	  				chartArea: {
				        width: '80%',
				        height: '80%',
				      },
					};
	
	  var chart = new google.visualization.PieChart(document.getElementById('piechart2'));
	  chart.draw(data, options);
	}	
}

function hideKrvodajalci() {
	document.getElementById("krvodajalci").style.display = "none";	
	
	// Pocisti tabelo
	var stVrstic = $("#tabelaKrvodajalcev tr").length;
	for (var i = stVrstic - 1; i > 0; i--) {
		document.getElementById("tabelaKrvodajalcev").deleteRow(i);
	}
	
	document.getElementById("obvestiloDaNiUjemanj").innerHTML = "";
}

var ehrPrejemnikaTemp;
var imePrejemnikaTemp;
var priimekPrejemnikaTemp;
var spolPrejemnikaTemp;
var tipPrejemnikaTemp;

var typeA = 0;
var typeB = 0;
var typeAB = 0;
var type0 = 0;


var allEHRids = [];

function poisciKrvodajalca() {
	document.getElementById("krvodajalci").style.display = "block";
	document.getElementById("obvestiloDaNiUjemanj").style.display = "none";
	
	if (spolPrejemnikaTemp === "Moški")
		document.getElementById("potencialniZa").innerHTML = imePrejemnikaTemp + "-a " + priimekPrejemnikaTemp + "-a";
	else
		document.getElementById("potencialniZa").innerHTML = imePrejemnikaTemp + "-o " + priimekPrejemnikaTemp;
	
	sessionId = getSessionId();
	
	// Pocisti tabelo
	
	var stVrstic = $("#tabelaKrvodajalcev tr").length;
	for (var i = stVrstic - 1; i > 0; i--) {
		document.getElementById("tabelaKrvodajalcev").deleteRow(i);
	}
	
	var wait = false;
	var najden = false;
	
	for (i in allEHRids) {
		if (i == (allEHRids.length - 1)) { 
			wait = true;
		}
		
		ehrId = allEHRids[i];
		
		if (ehrId != ehrPrejemnikaTemp)
			$.ajax({
				url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
				type: 'GET',
				headers: {"Ehr-Session": sessionId},
		    	success: function (data) {
					var party = data.party;

				    var today = new Date();
				    var birthDate = new Date(party.dateOfBirth);
				    var age = today.getFullYear() - birthDate.getFullYear();
				    var m = today.getMonth() - birthDate.getMonth();
				    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
				        age--;
				    }
		    		
		    		var tip, kontakt;
		    		
		    		for (j in party.partyAdditionalInfo) {
		            	if (party.partyAdditionalInfo[j].key === 'bloodType') {
		                	tip = party.partyAdditionalInfo[j].value;
		                break;
		            	}
		        	}
		        	
		        	for (j in party.partyAdditionalInfo) {
		            	if (party.partyAdditionalInfo[j].key === 'phone') {
		                	kontakt = party.partyAdditionalInfo[j].value;
		                break;
		            	}
		        	}
		        	
		        	// Izpisemo glede na tip krvi
		        	var izpis = false;
		        	switch(tipPrejemnikaTemp) {
		        		case "A":
		        			if (tip === "0" || tip === "A") izpis = true;
		        			break;
		        		case "B":
		        			if (tip === "0" || tip === "B") izpis = true;
		        			break;
		        		case "AB":
		        			izpis = true;
		        			break;
		        		case "0":
		        			if (tip === "0") izpis = true;
		        			break;
		        	}	
		        	
		        	if (izpis) {
		        		najden = true;
			        	var table = document.getElementById("tabelaKrvodajalcev");
					    var row = table.insertRow();
					    var cell1 = row.insertCell(0);
					    var cell2 = row.insertCell(1);
					    var cell3 = row.insertCell(2);
					    var cell4 = row.insertCell(3);
					    
					    cell1.innerHTML = party.firstNames + " " + party.lastNames;
	    				cell2.innerHTML = age + " let";
	    				cell3.innerHTML = tip;
	    				cell4.innerHTML = kontakt;
		        	}
		        	else if (!najden && wait) {
						document.getElementById("obvestiloDaNiUjemanj").style.display = "block";
		        	}
				},
			});
	}
}


// Prikaži sliko vseh kombinacij pri darovanju krvi
function prikaziKombinacije() {
	window.open("https://www.donateblood.com.au/sites/default/files/Blood%20type%20table.gif");
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajOsebe() {
    for(var i=1; i<=3; i++){
    	generirajPodatke(i);
    }
	
	izris();
	
    var bar = document.getElementById("snackbar");
    bar.className = "show";
    setTimeout(function(){ 
    	bar.className = bar.className.replace("show", ""); 
    }, 3000);
}
 
function generirajPodatke(stOsebe) {
	var sessionId = getSessionId();
	
	var ehrId = "";
	var ime  = "";
	var priimek  = "";
	var spol = "";
	var datumRojstva = "";
	var krvnaSkupina = "";
	var kontakt = "";
    switch(stOsebe){
    	case 1:
            ime = "Janez";
            priimek = "Novak";
            spol = "Moški";
            datumRojstva = "1999-04-04";
            krvnaSkupina = "A";
            typeA++;
            kontakt = "040-123-456";
    		break;
    	case 2:
            ime = "Ana";
            priimek = "Horvat";
            spol = "Ženski";
            datumRojstva = "1990-12-09";
            krvnaSkupina = "0";
            type0++;
            kontakt = "050-123-456";
    		break;
    	case 3:
            ime = "Peter";
            priimek = "Kovač";
            spol = "Moški";
            datumRojstva = "1982-01-02";
            krvnaSkupina = "AB";
            typeAB++;
            kontakt = "060-123-456";
    		break;
	}
	$.ajaxSetup({
		headers: {"Ehr-Session": sessionId}
	});
	$.ajax({
	url: baseUrl + "/ehr",
	type: 'POST',
	success: function(data) {
      ehrId = data.ehrId;
      var partyData = {
        firstNames: ime,
        lastNames: priimek,
        dateOfBirth: datumRojstva,
        partyAdditionalInfo: [{key: "ehrId", value: ehrId},
        						{key: "sex", value: spol},
		                        {key: "bloodType", value: krvnaSkupina},
		                        {key: "phone", value: kontakt}]
      };
      $.ajax({
        url: baseUrl + "/demographics/party",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(partyData),
        success: function(party) {
			$("#preberiObstojeciEHR").append("<option value="+ehrId+">"+ime+" "+priimek+"</option");
        }
      });
      allEHRids.push(ehrId);
	}
  });
  return ehrId;
}


/**
 * Kreiraj nov EHR zapis za pacienta in dodaj osnovne demografske podatke.
 * V primeru uspešne akcije izpiši sporočilo s pridobljenim EHR ID, sicer
 * izpiši napako.
 */
function kreirajEHRzaOsebo() {
    sessionId = getSessionId();
    
    var ime = $("#kreirajIme").val();
    var priimek = $("#kreirajPriimek").val();
    var datumRojstva = $("#kreirajDatumRojstva").val() + "T00:00:00.000Z";
    var spol = $("#kreirajSpol input:radio:checked").val();
    var krvnaSkupina = $("#kreirajKrvnoSkupino input:radio:checked").val();
	var kontakt = $("#kreirajKontakt").val();
	
	if (!ime || !priimek || !datumRojstva || !spol || !krvnaSkupina || !kontakt ||
	  ime.trim().length == 0 || priimek.trim().length == 0 || datumRojstva.trim().length == 0 || kontakt.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		switch(krvnaSkupina) {
			case "A":
				typeA++;
				break;
			case "B":
				typeB++;
				break;
			case "AB":
				typeAB++;
				break;
			case "0":
				type0++;
				break;
		}
		
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId},
		                                  {key: "sex", value: spol},
		                                  {key: "bloodType", value: krvnaSkupina},
		                                  {key: "phone", value: kontakt}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
	                          "label label-success fade-in'>Uspešno kreiran EHR '" +
	                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                	
		                }
		                $("#preberiObstojeciEHR").append("<option value="+ehrId+">"+ime+" "+priimek+"</option");
		                allEHRids.push(ehrId);
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
		                    "label-danger fade-in'>Napaka '" +
		                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		        $("#kreirajIme").val('');
			    $("#kreirajPriimek").val('');
			    $("#kreirajDatumRojstva").val('');
			    $("#kreirajSpol input:radio:checked").prop('checked', false);
			    $("#kreirajKrvnoSkupino input:radio:checked").prop('checked', false);
			    $("#kreirajKontakt").val("");
			    izris();
		    }
		});
	}
}


/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preberiEHRzaOsebo() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		document.getElementById("patient-name").innerHTML = "";
		document.getElementById("patient-surname").innerHTML = "";
		document.getElementById("patient-sex").innerHTML = "";
		document.getElementById("patient-birthDate").innerHTML = "";
		document.getElementById("patient-bloodType").innerHTML = "";
		document.getElementById("patient-phone").innerHTML = "";
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				
				var today = new Date();
			    var birthDate = new Date(party.dateOfBirth);
			    var age = today.getFullYear() - birthDate.getFullYear();
			    var m = today.getMonth() - birthDate.getMonth();
			    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
			        age--;
				}
				
				ehrPrejemnikaTemp = ehrId;
				document.getElementById("osnovno").style.display = "block";
        		document.getElementById("patient-name").innerHTML = party.firstNames;
        		imePrejemnikaTemp = party.firstNames;
        		document.getElementById("patient-surname").innerHTML = party.lastNames;
        		priimekPrejemnikaTemp = party.lastNames;
				for (j in party.partyAdditionalInfo) {
                	if (party.partyAdditionalInfo[j].key === 'sex') {
                    	document.getElementById("patient-sex").innerHTML = party.partyAdditionalInfo[j].value;
                    	spolPrejemnikaTemp = party.partyAdditionalInfo[j].value;
                    break;
                	}
            	}
        		document.getElementById("patient-birthDate").innerHTML = party.dateOfBirth + "  [Starost: " + age + " let]";
        		for (j in party.partyAdditionalInfo) {
                	if (party.partyAdditionalInfo[j].key === 'bloodType') {
                    	document.getElementById("patient-bloodType").innerHTML = party.partyAdditionalInfo[j].value;
                    	tipPrejemnikaTemp = party.partyAdditionalInfo[j].value;
                    break;
                	}
            	}
            	for (j in party.partyAdditionalInfo) {
                	if (party.partyAdditionalInfo[j].key === 'phone') {
                    	document.getElementById("patient-phone").innerHTML = party.partyAdditionalInfo[j].value;
                    break;
                	}
            	}
			},
		});
		$("#preberiEHRid").val('');
	}
}

$(document).ready(function() {
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});
});